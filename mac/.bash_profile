### Added by the Heroku Toolbelt
export PATH="/usr/local/heroku/bin:$PATH"

##
# Your previous /Users/ygueron/.bash_profile file was backed up as /Users/ygueron/.bash_profile.macports-saved_2015-05-18_at_18:08:13
##

# MacPorts Installer addition on 2015-05-18_at_18:08:13: adding an appropriate PATH variable for use with MacPorts.
export PATH="/opt/local/bin:/opt/local/sbin:$PATH"
# Finished adapting your PATH environment variable for use with MacPorts.

# The following was added by Yann GUERON

# Mes alias usuels
alias ls='ls -G'
alias gs='git status'
alias ga='git add'
alias gb='git branch'
alias gc='git commit'
alias gd='git diff'
alias go='git checkout'
alias gk='gitk --all&'
alias gx='gitx --all'

#Normalement, c'est utilisé par git pour éditer les commit en particulier.
export EDITOR="vim"

# J'aime avoir mon propre prompt aussi.
LSCOLORS="DxGxFxdxCxdxdxhbadExEx"
export LSCOLORS

# BLACK=$(tput setaf 0)
# RED=$(tput setaf 1)
# GREEN=$(tput setaf 2)
# YELLOW=$(tput setaf 3)
# LIME_YELLOW=$(tput setaf 190)
# POWDER_BLUE=$(tput setaf 153)
# BLUE=$(tput setaf 4)
# MAGENTA=$(tput setaf 5)
# CYAN=$(tput setaf 6)
# WHITE=$(tput setaf 7)
# NORMAL=$(tput sgr0)

BLACK='\[\e[0;30m\]'
DARKGRAY='\[\e[1;30m\]'
RED='\[\e[0;31m\]'
LIGHTRED='\[\e[1;31m\]'
GREEN='\[\e[0;32m\]'
LIGHTGREEN='\[\e[1;32m\]'
BROWN='\[\e[0;33\]'
YELLOW='\[\e[1;33\]'
BLUE='\[\e[0;34m\]'
LIGHTBLUE='\[\e[1;34m\]'
PURPLE='\[\e[0;35m\]'
LIGHTPURPLE='\[\e[1;35m\]'
CYAN='\[\e[0;36m\]'
LIGHTGRAY='\[\e[0;37m\]'
WHITE='\[\e[1;37m\]'

# create horizontal line that fills the middle of the terminal with dynamic width
function horizline {
    TERMWIDTH=${COLUMNS}
    let promptsize=$(echo -n "-[\u]-[\h]-[${PWD}:]" | wc -c | tr -d " ")
    let fillsize=${TERMWIDTH}-${promptsize}
    fill=""
    while [ "$fillsize" -gt "0" ]
    do
fill="${fill}-"
        let fillsize=${fillsize}-1
    done
echo ${fill}
}

PROMPT1="$LIGHTPURPLE-[$LIGHTPURPLE\h$LIGHTPURPLE]-[$LIGHTPURPLE\w:$LIGHTPURPLE]"
PROMPT2="$LIGHTPURPLE-"
PS1="$PROMPT1"'`horizline`'"$PROMPT2\n-$LIGHTGRAY$ "

#end added by YG
